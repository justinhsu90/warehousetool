package jxdeveloping.warehousetool;

import android.text.TextUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by justinhsu on 24/05/2017.
 */
public class MyConfig {

    private static String sIP = "";
    private static final String SKUNotUseText = "NotUse";
    private static final String defaultXMLFileNamePrefix = "RESULTTEMPLATE";
    private static final boolean isDALEBRONAS = true;
    private static final boolean isProduction = false;
    private static final boolean isMAC = true;
    private static final boolean useProxy = true;

    private static final String accountNameFlybuddy = "Flybuddy";
    private static final String accountNameFlybuddyShort = "FB";
    private static final String accountNameDalTech = "DalTech";
    private static final String accountNameDalTechShort = "DT";

    private static final String proxyURL = "127.0.0.1";
    private static final String proxyPort = "1089";


    /**
     * Country List
     **/

    private static List<String> accountList = Arrays.asList(new String[]{"DalTech", "Flybuddy"});
    private static List<String> EUCountryList = Arrays.asList(new String[]{"UK", "ES", "IT", "FR", "DE"});
    private static List<String> NACountryList = Arrays.asList(new String[]{"US", "CA", "MX"});

    /**
     * runtime data
     **/
    private static String currentIP = "";
    private static String currentDate = "";
    private static String currentVPSIP = "";
    private static String currentDatabaseIP = "";
    private static String currentEndpoint = "";
    private static String currentSellerId = "";
    private static String currentAccessKeyId = "";
    private static String currentSecretKey = "";
    private static String currentMarketPlaceId = "";
    private static String currentDeveloperAccountNumber = "";
    private static String currentAppName = "";
    private static String currentAppVersion = "";
    private static String currentAccount = "";
    private static String currentCountry = "";


    /**
     * WowcherAPI
     */
    private static final String wowcherApiStaging = "http://api.staging.redemption.wowcher.co.uk";
    private static final String wowcherApiProduction = "https://api.redemption.wowcher.co.uk";
    private static final String wowcherStagingSecret = "f845e4d62e8cd2efe4563f440aacd96c";
    private static final String wowcherStagingKey = "9ab017be1373e98a0fa44bc931a8b74c";
    private static final String wowcherStagingAuthenticationBase64Encode = "OWFiMDE3YmUxMzczZTk4YTBmYTQ0YmM5MzFhOGI3NGM6Zjg0NWU0ZDYyZThjZDJlZmU0NTYzZjQ0MGFhY2Q5NmM=";

    private static final String wowcherProductionSecret = "37fc15a040c8f98266482ff7a1da6ee1";
    private static final String wowcherProductionKey = "a83612743faaf9ca17a1fab2a08b49cb";
    private static final String wowcherProductionAuthenticationBase64Encode = "YTgzNjEyNzQzZmFhZjljYTE3YTFmYWIyYTA4YjQ5Y2I6MzdmYzE1YTA0MGM4Zjk4MjY2NDgyZmY3YTFkYTZlZTE=";


    /**
     * DalTech IP
     */
    private static final String dalTechVPSIP = "23.83.229.26";
    private static final String dalTechOfficeIP = "61.216.178.48";

    /**
     * VPS Flybuddy IPs
     */
    private static final String flybuddyVPSIP = "118.193.233.116";
    private static final String flybuddyOfficeIP = "61.216.178.44";
    private static final String flybuddyOfficeIP2 = "61.216.178.45";
    /**
     * Database UFO
     */
    private static final int databasePort_UFO = 3306;
    private static final String dataBaseRemoteIP_UFO = "118.193.233.116";
    private static final String dataBaseLocalIP_UFO = "127.0.0.1";
    private static final String databaseUsername_UFO = "flybuddy";
    private static final String databasePassword_UFO = "Fbtw.2016";
    private static final String databaseDBName_UFO = "CMS";

    /**
     * Database Office
     */
    private static final int currentDatabasePort_Office = 3307;
    private static final String dateBaseRemoteIP_Office = "61.216.178.44";
    //    private static  String dateBaseLocalIP_Office = "127.0.0.1";
    private static final String databaseUsername_Office_DalTech = "vmdaltech";
    private static final String databaseUsername_Office_Flybuddy = "vmflybuddy";
    private static final String databasePassword_Office = "Fbtw.2016";
    private static final String databaseDBName_Office = "CMS";

    /**
     * Database Runtime data
     */
    private static int currentDatabasePort;
    private static String currentDateBaseRemoteIP = "";
    private static String currentDateBaseLocalIP = "";
    private static String currentDatabaseUsername = "";
    private static String currentDatabasePassword = "";
    private static String currentDatabaseDBName = "";

    /**
     * Database Table Name
     **/
    private static String currentDatabaseTable_CurrencyTable = "";
    private static String currentDatabaseTable_FBAWarehouseIDList = "";
    private static String currentDatabaseTable_InboundShipmentItemsList = "";
    private static String currentDatabaseTable_PackingInfo = "";
    private static String currentDatabaseTable_InboundShipmentList = "";
    private static String currentDatabaseTable_ContentList = "";
    private static String currentDatabaseTable_FinancialOrderItemsList = "";
    private static String currentDatabaseTable_OrderItemsList = "";
    private static String currentDatabaseTable_Log = "";
    private static String currentDatabaseTable_OrdersList = "";
    private static String currentDatabaseTable_FBAOrdersList = "";
    private static String currentDatabaseTable_MFNOrdersList = "";
    private static String databaseTable_BuyerInfoList = "";
    private static String currentDatabaseTable_BuyerInfoList = "";
    private static String currentDatabaseTable_OverseaWarehouse = "";
    private static String currentDatabaseTable_OverseaWarehouseInventory = "";
    private static String currentDatabaseTable_WowcherOrder = "";
    private static String currentDatabaseTable_DailyInventoryList = "";
    private static String databaseTable_DailyInventoryList = "";
    private static String currentDatabaseTable_RestockRecord = "";
    private static String currentDatabaseTable_RestockRecordBySKU = "";
    private static String currentDatabaseTable_SKUCategoryList = "";
    private static String currentDatabaseTable_DeprecatedSKUList = "";
    private static String currentDatabaseTable_ListingUpdateList = "";
    private static String currentDatabaseTable_SKU_UPC_List = "";
    private static String currentDatabaseTable_FeedXmlList = "";
    private static String currentDatabaseTable_ProductCostList = "";
    private static String currentWowcherAuthentication = "";
    private static String currentWowcherURL = "";


    /**
     * MWS API Request Time period
     **/
    private static final long financialOrderItemsListFetchTimeGap = TimeUnit.SECONDS.toMillis(10);
    private static final long shipmentListFetchTimeGap = TimeUnit.SECONDS.toMillis(10);
    private static final long shipmentItemsListFetchTimeGap = TimeUnit.SECONDS.toMillis(5);
    private static final long orderItemsListFetchTimeGap = TimeUnit.SECONDS.toMillis(3);
    private static final long orderListFetchTimeGap = TimeUnit.SECONDS.toMillis(65);
    private static final long requestReportInventoryTimeGap = TimeUnit.SECONDS.toMillis(20);
    //    private static  long OrderListFetchTimeGap = TimeUnit.SECONDS.toMillis(12);
    private static final long requestReportOrderTimeGap = TimeUnit.SECONDS.toMillis(30);
    private static final long requestReportTimeGap = TimeUnit.SECONDS.toMillis(30);
    private static final long reportRequestRetryTimeGap = TimeUnit.SECONDS.toMillis(30);
    private static final long feedSubmissionResultRequestRetryTimeGap = TimeUnit.SECONDS.toMillis(80);
    private static final int reportRequestRetryTime = 10;

    /**
     * Restock Calculate parameters
     */
    private static final int purchasingDays = 4;
    private static final int airShippingDays = 16;
    private static final int restockDays = purchasingDays + airShippingDays;

    /**
     * Flybuddy MWS API North America only
     **/
    private static final String flybuddyNASellerId = "A2NSJBBBCMCR";
    private static final String flybuddyNAAccessKeyId = "AKIAIC3CAZ2HWGU7AGZQ";
    private static final String flybuddyNASecretKey = "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";
    private static final String flybuddyNADeveloperAccountNumber = "0030-3701-3951";
    private static final String flybuddyNAAppVerion = "1";

    /**
     * Flybuddy MWS API Europe only
     **/
    private static final String flybuddyEUSellerId = "A32LL306HN45LI";
    private static final String flybuddyEUAccessKeyId = "AKIAILU5XWARAIBWLRRA";
    private static final String flybuddyEUSecretKey = "YpkFcHLJjOkc2kC7y5+MUVmnAcIt5gliGllf/rA";
    private static final String flybuddyEUDeveloperAccountNumber = "5830-2393-0441";
    private static final String flybuddyEUAppVerion = "1";

    /**
     * DalTech MWS NA API  only
     **/
    private static final String dalTechNASellerId = "A2WNQX87ZU867W";
    private static final String dalTechNAAccessKeyId = "AKIAIMDSTPSH3LZTUW6A";
    private static final String dalTechNASecretKey = "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";
    private static final String dalTechNADeveloperAccountNumber = "7085-6740-1772";
    private static final String dalTechNAAppVerion = "1";

    /**
     * DalTech MWS EU API  only
     **/
    private static final String dalTechEUSellerId = "A1QT3IZ3Q3A15Q";
    private static final String dalTechEUAccessKeyId = "AKIAJ62U4YSQIAGRG76Q";
    private static final String dalTechEUSecretKey = "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";
    private static final String dalTechEUDeveloperAccountNumber = "0062-1757-7632";
    private static final String dalTechEUAppVerion = "1";

    /**
     * MWS API query parameters
     **/
    private static final String inventoryDetailed = "Detailed";

    public boolean isMySQL() {
        return currentDatabaseIP == dataBaseRemoteIP_UFO ? true : false;
    }

    public void setCountryAccountIP(String accountCountry) throws Exception {
//        setAccountAndCountry(accountCountry);
        setDBFromDevelopmentOrProduction();
        if (isProduction()) {
            switch (sIP) {
                case dalTechOfficeIP:
                    currentDatabaseUsername = databaseUsername_Office_DalTech;
                    currentDatabaseIP = dateBaseRemoteIP_Office;
                    currentDatabasePassword = databasePassword_Office;
                    currentDatabaseDBName = databaseDBName_Office;
                    currentDatabasePort = currentDatabasePort_Office;
                    break;
                case flybuddyOfficeIP:
                case flybuddyOfficeIP2:
                    currentDatabaseIP = dateBaseRemoteIP_Office;
                    currentDatabasePort = currentDatabasePort_Office;
                    currentDatabaseUsername = databaseUsername_Office_Flybuddy;
                    currentDatabasePassword = databasePassword_Office;
                    currentDatabaseDBName = databaseDBName_Office;
                    break;
                case dalTechVPSIP:
                case flybuddyVPSIP:
                    currentDatabaseIP = dataBaseRemoteIP_UFO;
                    currentDatabasePort = databasePort_UFO;
                    currentDatabaseUsername = databaseUsername_UFO;
                    currentDatabasePassword = databasePassword_UFO;
                    currentDatabaseDBName = databaseDBName_UFO;
                    break;
                default:
                    throw new Exception("improper IP");
            }
        } else if (!isProduction()) {
            if (isDALEBRONAS) {
                currentDatabaseIP = dateBaseRemoteIP_Office;
                currentDatabasePort = currentDatabasePort_Office;
                currentDatabaseUsername = "master";
                currentDatabasePassword = databasePassword_Office;
                currentDatabaseDBName = databaseDBName_Office;
            } else {
                currentDatabaseIP = dataBaseRemoteIP_UFO;
                currentDatabasePort = databasePort_UFO;
                currentDatabaseUsername = databaseUsername_UFO;
                currentDatabasePassword = databasePassword_UFO;
                currentDatabaseDBName = databaseDBName_UFO;
            }
        } else {
            throw new Exception("Either Local, Remote, Account,Country is not set yet!!");
        }
    }

    public boolean isProduction() {
        return isProduction && !isMAC;
    }

    private static void setDBFromDevelopmentOrProduction() {
//        if (isProduction()) {
        if (true) {
            currentDatabaseTable_OrderItemsList = "OrderItemsList";
            currentDatabaseTable_Log = "Log";
            currentDatabaseTable_OrdersList = "OrdersList";
            currentDatabaseTable_FBAOrdersList = "FBAOrdersList";
            currentDatabaseTable_MFNOrdersList = "MFNOrdersList";
            currentDatabaseTable_BuyerInfoList = "BuyerInfoList";
            currentDatabaseTable_DailyInventoryList = "DailyInventoryList";
            currentDatabaseTable_RestockRecord = "RestockRecord";
            currentDatabaseTable_RestockRecordBySKU = "RestockRecordBySKU";
            currentDatabaseTable_SKUCategoryList = "SKUCategoryList";
            currentDatabaseTable_DeprecatedSKUList = "DeprecatedSKUList";
            currentDatabaseTable_FinancialOrderItemsList = "FinancialOrderItemsList";
            currentDatabaseTable_OverseaWarehouse = "OverseaWarehouse";
            currentDatabaseTable_WowcherOrder = "WowcherOrderList";
            currentDatabaseTable_InboundShipmentList = "InboundShipmentList";
            currentDatabaseTable_InboundShipmentItemsList = "InboundShipmentItemsList";
            currentDatabaseTable_FBAWarehouseIDList = "FBAWarehouseIDList";
            currentDatabaseTable_PackingInfo = "PackingInfo";
            currentDatabaseTable_CurrencyTable = "CurrencyTable";
            currentDatabaseTable_ContentList = "ContentList";
            currentDatabaseTable_ListingUpdateList = "ListingUpdateStatus";
            currentDatabaseTable_SKU_UPC_List = "SkuUpcList";
            currentDatabaseTable_FeedXmlList = "FeedXmlList";
            currentDatabaseTable_ProductCostList = "ProductCost";
            currentDatabaseTable_OverseaWarehouseInventory = "OverseaWarehouseDailyInventory";

        } else {
            currentDatabaseTable_OrderItemsList = "OrderItemsList" + "_Test";
            currentDatabaseTable_Log = "Log" + "_Test";
            currentDatabaseTable_OrdersList = "OrdersList" + "_Test";
            currentDatabaseTable_FBAOrdersList = "FBAOrdersList" + "_Test";
            currentDatabaseTable_MFNOrdersList = "MFNOrdersList" + "_Test";
            currentDatabaseTable_BuyerInfoList = "BuyerInfoList" + "_Test";
            currentDatabaseTable_OverseaWarehouse = "OverseaWarehouse" + "_Test";
            currentDatabaseTable_WowcherOrder = "WowcherOrderList" + "_Test";
            currentDatabaseTable_DailyInventoryList = "DailyInventoryList" + "_Test";
            currentDatabaseTable_RestockRecord = "RestockRecord" + "_Test";
            currentDatabaseTable_RestockRecordBySKU = "RestockRecordBySKU" + "_Test";
            currentDatabaseTable_SKUCategoryList = "SKUCategoryList" + "_Test";
            currentDatabaseTable_DeprecatedSKUList = "DeprecatedSKUList" + "_Test";
            currentDatabaseTable_FinancialOrderItemsList = "FinancialOrderItemsList" + "_Test";
            currentDatabaseTable_InboundShipmentList = "InboundShipmentList";
            currentDatabaseTable_InboundShipmentItemsList = "InboundShipmentItemsList";
            currentDatabaseTable_FBAWarehouseIDList = "FBAWarehouseIDList";
            currentDatabaseTable_PackingInfo = "PackingInfo";
            currentDatabaseTable_CurrencyTable = "CurrencyTable";
            currentDatabaseTable_ContentList = "ContentList";
            currentDatabaseTable_ListingUpdateList = "ListingUpdateStatus";
            currentDatabaseTable_SKU_UPC_List = "SkuUpcList";
            currentDatabaseTable_FeedXmlList = "FeedXmlList";
            currentDatabaseTable_ProductCostList = "ProductCost";
            currentDatabaseTable_OverseaWarehouseInventory = "OverseaWarehouseDailyInventory";

        }
    }

    public enum MWSEndpoint {

        US("mws.amazonservices.com"),
        UK("mws.amazonservices.co.uk"),
        DE("mws.amazonservices.de"),
        FR("mws.amazonservices.fr"),
        JP("mws.amazonservices.jp"),
        CN("mws.amazonservices.com.cn"),
        IT("mws.amazonservices.it"),
        MX("mws.amazonservices.com.mx"),
        ES("mws.amazonservices.es"),
        CA("mws.amazonservices.ca");

        private String url;

        MWSEndpoint(String domain) {
            this.url = "https://" + domain + "/";
        }

        @Override
        public String toString() {
            return url;
        }

    }

    public enum MWSRequestType {

        ListOrder("Orders/2013-09-01"),
        ListInventory("FulfillmentInventory/2010-10-01");

        private String type;

        MWSRequestType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    public String getEndPoint(String country, MWSRequestType type) {
        if (!TextUtils.isEmpty(country) && type != null) {
            return country + type.toString();
        }
        return "";
    }

    public enum UpdateToDBType {
        OrdersListFromServer("OrdersListFromServer"),
        OrderItemsListFromServer("OrderItemsListFromServer"),
        FinancialNormalOrderItemsListFromServer("FinancialNormalOrderItemsListFromServer"),
        FinancialRefundOrderItemsListFromServer("FinancialRefundOrderItemsListFromServer"),
        InventoryDailyStockFromServer("InventoryDailyStockFromServer"),
        ShipmentItemsListFromServer("ShipmentItemsListFromServer"),
        ShipmentListFromServer("ShipmentListFromServer");
        private String type;

        UpdateToDBType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    public enum marketPlaceId {
        US("ATVPDKIKX0DER"),
        CA("A2EUQ1WTGCTBG2"),
        UK("A1F83G8C2ARO7P"),
        DE("A1PA6795UKMFR9"),
        FR("A13V1IB3VIYZZH"),
        ES("A1RKKUPIHCS9HS"),
        IT("APJ6JRA9NG5V4"),
        MX("A1AM78C64UM0Y8"),
        JP("A1VC38T7YXB528"),
        NON("");

        private String Country;

        marketPlaceId(String type) {
            this.Country = type;
        }

        @Override
        public String toString() {
            return Country;
        }
    }

    public enum shippingType {
        AIR("AIR"),
        SEA("SEA"),
        AIREXPRESS("AIREXPRESS"),
        SEAEXPRESS("SEAEXPRESS");

        private String type;

        shippingType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    public enum MWSReportType {
        Stock_FBA("_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_"),
        Order_FBA("_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_"),
        Order_MFN("_GET_CONVERGED_FLAT_FILE_ORDER_REPORT_DATA_"),
        All_Listing("_GET_MERCHANT_LISTINGS_ALL_DATA_"),
        Shipment_FBA("_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_"),
        Update_Content("_POST_PRODUCT_DATA_"),
        Update_Price("_POST_PRODUCT_PRICING_DATA_"),
        NON("");

        private String reportType;

        MWSReportType(String type) {
            this.reportType = type;
        }

        @Override
        public String toString() {
            return reportType;
        }

    }

    public enum UpdateProductTypePath {
        PRICE("src/main/java/com/main/ContentUpdate/updatePrice.xml"),
        CONTENT("src/main/java/com/main/ContentUpdate/updateContent.xml"),
        NON("");

        private String reportType;

        UpdateProductTypePath(String type) {
            this.reportType = type;
        }

        @Override
        public String toString() {
            return reportType;
        }

    }

    public enum WowcherURL {

        STAGING("http://api.staging.redemption.wowcher.co.uk"),
        PRODUCTION("https://api.redemption.wowcher.co.uk");

        private String url;

        WowcherURL(String reportType) {
            this.url = reportType;
        }

        @Override
        public String toString() {
            return url;
        }

    }


    public enum WowcherRequestType {

        Order("/v1/orders"),
        Update("/v1/orders/status"),
        Echo("/v1/echo");

        private String url;

        WowcherRequestType(String reportType) {
            this.url = reportType;
        }

        @Override
        public String toString() {
            return url;
        }

    }


    private static void setMarketPlaceIdAndEndpoint(String accountCountry) {
        switch (accountCountry) {
            case "DT_CA":
            case "FB_CA":
                currentCountry = "CA";
                currentEndpoint = MWSEndpoint.CA.toString();
                currentMarketPlaceId = marketPlaceId.CA.toString();
                break;
            case "DT_US":
            case "FB_US":
                currentCountry = "US";
                currentEndpoint = MWSEndpoint.US.toString();
                currentMarketPlaceId = marketPlaceId.US.toString();
                break;
            case "DT_MX":
            case "FB_MX":
                currentCountry = "MX";
                currentEndpoint = MWSEndpoint.MX.toString();
                currentMarketPlaceId = marketPlaceId.MX.toString();
                break;
            case "DT_UK":
            case "FB_UK":
                currentCountry = "UK";
                currentEndpoint = MWSEndpoint.UK.toString();
                currentMarketPlaceId = marketPlaceId.UK.toString();
                break;
            case "DT_DE":
            case "FB_DE":
                currentCountry = "DE";
                currentEndpoint = MWSEndpoint.DE.toString();
                currentMarketPlaceId = marketPlaceId.DE.toString();
                break;
            case "DT_FR":
            case "FB_FR":
                currentCountry = "FR";
                currentEndpoint = MWSEndpoint.FR.toString();
                currentMarketPlaceId = marketPlaceId.FR.toString();
                break;
            case "DT_ES":
            case "FB_ES":
                currentCountry = "ES";
                currentEndpoint = MWSEndpoint.ES.toString();
                currentMarketPlaceId = marketPlaceId.ES.toString();
                break;
            case "DT_IT":
            case "FB_IT":
                currentCountry = "IT";
                currentEndpoint = MWSEndpoint.IT.toString();
                currentMarketPlaceId = marketPlaceId.IT.toString();
                break;
        }
    }

    private static void setAccount(String accountCountry) {
        switch (accountCountry) {
            case "DT_US":
            case "DT_MX":
            case "DT_CA":
                currentAppName = "DalTech";
                currentAccount = "DalTech";
                currentAppVersion = dalTechNAAppVerion;
                currentAccessKeyId = dalTechNAAccessKeyId;
                currentSecretKey = dalTechNASecretKey;
                currentSellerId = dalTechNASellerId;
                break;
            case "DT_IT":
            case "DT_FR":
            case "DT_UK":
            case "DT_DE":
            case "DT_ES":
                currentAppName = "DalTech";
                currentAccount = "DalTech";
                currentAppVersion = dalTechEUAppVerion;
                currentAccessKeyId = dalTechEUAccessKeyId;
                currentSecretKey = dalTechEUSecretKey;
                currentSellerId = dalTechEUSellerId;
                break;
            case "FB_CA":
            case "FB_US":
            case "FB_MX":
                currentAppName = "Flybuddy";
                currentAccount = "Flybuddy";
                currentAppVersion = flybuddyNAAppVerion;
                currentAccessKeyId = flybuddyNAAccessKeyId;
                currentSecretKey = flybuddyNASecretKey;
                currentSellerId = flybuddyNASellerId;
                break;
            case "FB_ES":
            case "FB_IT":
            case "FB_DE":
            case "FB_FR":
            case "FB_UK":
                currentAppName = "Flybuddy";
                currentAccount = "Flybuddy";
                currentAppVersion = flybuddyEUAppVerion;
                currentAccessKeyId = flybuddyEUAccessKeyId;
                currentSecretKey = flybuddyEUSecretKey;
                currentSellerId = flybuddyEUSellerId;
                break;
        }
    }

    public void setAccountAndCountry(String accountCountry) {
        setAccount(accountCountry);
        setMarketPlaceIdAndEndpoint(accountCountry);
    }

    public boolean isDataSetted() {
        return !TextUtils.isEmpty(currentDatabaseIP) &&
                !TextUtils.isEmpty(currentEndpoint) &&
                !TextUtils.isEmpty(currentSellerId) &&
                !TextUtils.isEmpty(currentAccessKeyId) &&
                !TextUtils.isEmpty(currentSecretKey) &&
                !TextUtils.isEmpty(currentMarketPlaceId) &&
                !TextUtils.isEmpty(currentAppName);
    }

    public boolean isCurrentIPCorrect(String country) throws IOException {
        if (!isProduction()) {
            return true;
        }
        if (country.contains("DT")) {
            if (sIP.contains(dalTechVPSIP)) {
                currentVPSIP = dalTechVPSIP;
                return true;
            } else if (sIP.contains(dalTechOfficeIP)) {
                currentVPSIP = dalTechOfficeIP;
                return true;
            }
        } else if (country.contains("FB")) {
            if (sIP.contains(flybuddyVPSIP)) {
                currentVPSIP = flybuddyVPSIP;
                return true;
            } else if (sIP.contains(flybuddyOfficeIP)) {
                currentVPSIP = flybuddyOfficeIP;
                return true;
            }
        }
        return false;
    }

    public String getsIP() {
        return sIP;
    }

    public String getSKUNotUseText() {
        return SKUNotUseText;
    }

    public boolean isDALEBRONAS() {
        return isDALEBRONAS;
    }

    public boolean isMAC() {
        return isMAC;
    }

    public List<String> getAccountList() {
        return accountList;
    }

    public List<String> getEUCountryList() {
        return EUCountryList;
    }

    public List<String> getNACountryList() {
        return NACountryList;
    }

    public String getCurrentIP() {
        return currentIP;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public String getCurrentVPSIP() {
        return currentVPSIP;
    }

    public String getCurrentDatabaseIP() {
        return currentDatabaseIP;
    }

    public String getCurrentEndpoint() {
        return currentEndpoint;
    }

    public String getCurrentSellerId() {
        return currentSellerId;
    }

    public String getCurrentAccessKeyId() {
        return currentAccessKeyId;
    }

    public String getCurrentSecretKey() {
        return currentSecretKey;
    }

    public String getCurrentMarketPlaceId() {
        return currentMarketPlaceId;
    }

    public String getCurrentDeveloperAccountNumber() {
        return currentDeveloperAccountNumber;
    }

    public String getCurrentAppName() {
        return currentAppName;
    }

    public String getCurrentAppVersion() {
        return currentAppVersion;
    }

    public String getCurrentAccount() {
        return currentAccount;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }

    public String getDalTechVPSIP() {
        return dalTechVPSIP;
    }

    public String getDalTechOfficeIP() {
        return dalTechOfficeIP;
    }

    public String getFlybuddyVPSIP() {
        return flybuddyVPSIP;
    }

    public String getFlybuddyOfficeIP() {
        return flybuddyOfficeIP;
    }

    public String getFlybuddyOfficeIP2() {
        return flybuddyOfficeIP2;
    }

    public int getDatabasePort_UFO() {
        return databasePort_UFO;
    }

    public String getDataBaseRemoteIP_UFO() {
        return dataBaseRemoteIP_UFO;
    }

    public String getDataBaseLocalIP_UFO() {
        return dataBaseLocalIP_UFO;
    }

    public String getDatabaseUsername_UFO() {
        return databaseUsername_UFO;
    }

    public String getDatabasePassword_UFO() {
        return databasePassword_UFO;
    }

    public String getDatabaseDBName_UFO() {
        return databaseDBName_UFO;
    }

    public int getCurrentDatabasePort_Office() {
        return currentDatabasePort_Office;
    }

    public String getDateBaseRemoteIP_Office() {
        return dateBaseRemoteIP_Office;
    }

    public String getDatabaseUsername_Office_DalTech() {
        return databaseUsername_Office_DalTech;
    }

    public String getDatabaseUsername_Office_Flybuddy() {
        return databaseUsername_Office_Flybuddy;
    }

    public String getDatabasePassword_Office() {
        return databasePassword_Office;
    }

    public String getDatabaseDBName_Office() {
        return databaseDBName_Office;
    }

    public int getCurrentDatabasePort() {
        return currentDatabasePort;
    }

    public String getCurrentDateBaseRemoteIP() {
        return currentDateBaseRemoteIP;
    }

    public String getCurrentDateBaseLocalIP() {
        return currentDateBaseLocalIP;
    }

    public static String getCurrentDatabaseUsername() {
        return currentDatabaseUsername;
    }

    public static String getCurrentDatabasePassword() {
        return currentDatabasePassword;
    }

    public String getCurrentDatabaseDBName() {
        return currentDatabaseDBName;
    }

    public String getCurrentDatabaseTable_CurrencyTable() {
        return currentDatabaseTable_CurrencyTable;
    }

    public String getCurrentDatabaseTable_FBAWarehouseIDList() {
        return currentDatabaseTable_FBAWarehouseIDList;
    }

    public String getCurrentDatabaseTable_InboundShipmentItemsList() {
        return currentDatabaseTable_InboundShipmentItemsList;
    }

    public String getCurrentDatabaseTable_PackingInfo() {
        return currentDatabaseTable_PackingInfo;
    }

    public String getCurrentDatabaseTable_InboundShipmentList() {
        return currentDatabaseTable_InboundShipmentList;
    }

    public String getCurrentDatabaseTable_ContentList() {
        return currentDatabaseTable_ContentList;
    }

    public String getCurrentDatabaseTable_FinancialOrderItemsList() {
        return currentDatabaseTable_FinancialOrderItemsList;
    }

    public String getCurrentDatabaseTable_OrderItemsList() {
        return currentDatabaseTable_OrderItemsList;
    }

    public String getCurrentDatabaseTable_Log() {
        return currentDatabaseTable_Log;
    }

    public String getCurrentDatabaseTable_OrdersList() {
        return currentDatabaseTable_OrdersList;
    }

    public String getCurrentDatabaseTable_FBAOrdersList() {
        return currentDatabaseTable_FBAOrdersList;
    }

    public String getCurrentDatabaseTable_MFNOrdersList() {
        return currentDatabaseTable_MFNOrdersList;
    }

    public String getDatabaseTable_BuyerInfoList() {
        return databaseTable_BuyerInfoList;
    }

    public String getCurrentDatabaseTable_BuyerInfoList() {
        return currentDatabaseTable_BuyerInfoList;
    }

    public String getCurrentDatabaseTable_OverseaWarehouse() {
        return currentDatabaseTable_OverseaWarehouse;
    }

    public String getCurrentDatabaseTable_WowcherOrder() {
        return currentDatabaseTable_WowcherOrder;
    }

    public String getCurrentDatabaseTable_DailyInventoryList() {
        return currentDatabaseTable_DailyInventoryList;
    }

    public String getDatabaseTable_DailyInventoryList() {
        return databaseTable_DailyInventoryList;
    }

    public String getCurrentDatabaseTable_RestockRecord() {
        return currentDatabaseTable_RestockRecord;
    }

    public String getCurrentDatabaseTable_RestockRecordBySKU() {
        return currentDatabaseTable_RestockRecordBySKU;
    }

    public String getCurrentDatabaseTable_SKUCategoryList() {
        return currentDatabaseTable_SKUCategoryList;
    }

    public String getCurrentDatabaseTable_DeprecatedSKUList() {
        return currentDatabaseTable_DeprecatedSKUList;
    }

    public long getFinancialOrderItemsListFetchTimeGap() {
        return financialOrderItemsListFetchTimeGap;
    }

    public long getShipmentListFetchTimeGap() {
        return shipmentListFetchTimeGap;
    }

    public long getShipmentItemsListFetchTimeGap() {
        return shipmentItemsListFetchTimeGap;
    }

    public long getOrderItemsListFetchTimeGap() {
        return orderItemsListFetchTimeGap;
    }

    public long getOrderListFetchTimeGap() {
        return orderListFetchTimeGap;
    }

    public long getRequestReportInventoryTimeGap() {
        return requestReportInventoryTimeGap;
    }

    public long getRequestReportOrderTimeGap() {
        return requestReportOrderTimeGap;
    }

    public long getRequestReportTimeGap() {
        return requestReportTimeGap;
    }

    public long getReportRequestRetryTimeGap() {
        return reportRequestRetryTimeGap;
    }

    public int getReportRequestRetryTime() {
        return reportRequestRetryTime;
    }

    public int getPurchasingDays() {
        return purchasingDays;
    }

    public int getAirShippingDays() {
        return airShippingDays;
    }

    public int getRestockDays() {
        return restockDays;
    }

    public String getFlybuddyNASellerId() {
        return flybuddyNASellerId;
    }

    public String getFlybuddyNAAccessKeyId() {
        return flybuddyNAAccessKeyId;
    }

    public String getFlybuddyNASecretKey() {
        return flybuddyNASecretKey;
    }

    public String getFlybuddyNADeveloperAccountNumber() {
        return flybuddyNADeveloperAccountNumber;
    }

    public String getFlybuddyNAAppVerion() {
        return flybuddyNAAppVerion;
    }

    public String getFlybuddyEUSellerId() {
        return flybuddyEUSellerId;
    }

    public String getFlybuddyEUAccessKeyId() {
        return flybuddyEUAccessKeyId;
    }

    public String getFlybuddyEUSecretKey() {
        return flybuddyEUSecretKey;
    }

    public String getFlybuddyEUDeveloperAccountNumber() {
        return flybuddyEUDeveloperAccountNumber;
    }

    public String getFlybuddyEUAppVerion() {
        return flybuddyEUAppVerion;
    }

    public String getDalTechNASellerId() {
        return dalTechNASellerId;
    }

    public String getDalTechNAAccessKeyId() {
        return dalTechNAAccessKeyId;
    }

    public String getDalTechNASecretKey() {
        return dalTechNASecretKey;
    }

    public String getDalTechNADeveloperAccountNumber() {
        return dalTechNADeveloperAccountNumber;
    }

    public String getDalTechNAAppVerion() {
        return dalTechNAAppVerion;
    }

    public String getDalTechEUSellerId() {
        return dalTechEUSellerId;
    }

    public String getDalTechEUAccessKeyId() {
        return dalTechEUAccessKeyId;
    }

    public String getDalTechEUSecretKey() {
        return dalTechEUSecretKey;
    }

    public String getDalTechEUDeveloperAccountNumber() {
        return dalTechEUDeveloperAccountNumber;
    }

    public String getDalTechEUAppVerion() {
        return dalTechEUAppVerion;
    }

    public String getInventoryDetailed() {
        return inventoryDetailed;
    }

    public long getFeedSubmissionResultRequestRetryTimeGap() {
        return feedSubmissionResultRequestRetryTimeGap;
    }

    public String getCurrentDatabaseTable_ListingUpdateList() {
        return currentDatabaseTable_ListingUpdateList;
    }

    public void setCurrentDatabaseTable_ListingUpdateList(String currentDatabaseTable_ListingUpdateList) {
        this.currentDatabaseTable_ListingUpdateList = currentDatabaseTable_ListingUpdateList;
    }

    public String getAccountNameFlybuddy() {
        return accountNameFlybuddy;
    }

    public String getAccountNameFlybuddyShort() {
        return accountNameFlybuddyShort;
    }

    public String getAccountNameDalTech() {
        return accountNameDalTech;
    }

    public String getAccountNameDalTechShort() {
        return accountNameDalTechShort;
    }

    public String getCurrentDatabaseTable_SKU_UPC_List() {
        return currentDatabaseTable_SKU_UPC_List;
    }

    public String getCurrentDatabaseTable_FeedXmlList() {
        return currentDatabaseTable_FeedXmlList;
    }

    public void setCurrentDatabaseTable_FeedXmlList(String currentDatabaseTable_FeedXmlList) {
        this.currentDatabaseTable_FeedXmlList = currentDatabaseTable_FeedXmlList;
    }

    public String getDefaultXMLFileNamePrefix() {
        return defaultXMLFileNamePrefix;
    }

    public boolean isUseProxy() {
        return useProxy;
    }

    public String getProxyURL() {
        return proxyURL;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public String getCurrentWowcherURL() {
        return currentWowcherURL;
    }

    public void setCurrentWowcherURL(String currentWowcherURL) {

        if (currentWowcherURL.equals(WowcherURL.PRODUCTION.toString())) {
            this.currentWowcherURL = WowcherURL.PRODUCTION.toString();
            this.currentWowcherAuthentication = wowcherProductionAuthenticationBase64Encode;

        } else {
            this.currentWowcherURL = WowcherURL.STAGING.toString();
            this.currentWowcherAuthentication = wowcherStagingAuthenticationBase64Encode;

        }
    }

    public String getCurrentDatabaseTable_ProductCostList() {
        return currentDatabaseTable_ProductCostList;
    }

    public String getCurrentDatabaseTable_OverseaWarehouseInventory() {
        return currentDatabaseTable_OverseaWarehouseInventory;
    }

    public String getWowcherStagingAuthenticationBase64Encode() {
        return wowcherStagingAuthenticationBase64Encode;
    }

    public String getWowcherApiStaging() {
        return wowcherApiStaging;
    }

    public String getWowcherApiProduction() {
        return wowcherApiProduction;
    }

    public String getWowcherStagingSecret() {
        return wowcherStagingSecret;
    }

    public String getWowcherStagingKey() {
        return wowcherStagingKey;
    }

    public String getWowcherProductionSecret() {
        return wowcherProductionSecret;
    }

    public String getWowcherProductionKey() {
        return wowcherProductionKey;
    }

    public String getWowcherProductionAuthenticationBase64Encode() {
        return wowcherProductionAuthenticationBase64Encode;
    }

    public String getCurrentWowcherAuthentication() {
        return currentWowcherAuthentication;
    }


}
