package jxdeveloping.warehousetool;

/**
 * Created by justinhsu on 29/05/2017.
 */
public class WowcherObject {




    String RedeemedAt = "";
    String WowcherCode = "";
    String CustomerName = "";
    String HouseNumber = "";
    String AddressLine1 = "";
    String AddressLine2 = "";
    String City = "";
    String County = "";
    String Postcode = "";
    String Phone = "";
    String ProductName = "";
    String Spec = "";
    String Model = "";
    String TrackingNo = "";
    String OrderStatus = "";

    public String getRedeemedAt() {
        return RedeemedAt;
    }

    public void setRedeemedAt(String redeemedAt) {
        RedeemedAt = redeemedAt;
    }

    public String getWowcherCode() {
        return WowcherCode;
    }

    public void setWowcherCode(String wowcherCode) {
        WowcherCode = wowcherCode;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getHouseNumber() {
        return HouseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        HouseNumber = houseNumber;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        AddressLine2 = addressLine2;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getPostcode() {
        return Postcode;
    }

    public void setPostcode(String postcode) {
        Postcode = postcode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSpec() {
        return Spec;
    }

    public void setSpec(String spec) {
        Spec = spec;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getTrackingNo() {
        return TrackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        TrackingNo = trackingNo;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "WowcherObject{" +
                "RedeemedAt='" + RedeemedAt + '\'' +
                ", WowcherCode='" + WowcherCode + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", HouseNumber='" + HouseNumber + '\'' +
                ", AddressLine1='" + AddressLine1 + '\'' +
                ", AddressLine2='" + AddressLine2 + '\'' +
                ", City='" + City + '\'' +
                ", County='" + County + '\'' +
                ", Postcode='" + Postcode + '\'' +
                ", Phone='" + Phone + '\'' +
                ", ProductName='" + ProductName + '\'' +
                ", Spec='" + Spec + '\'' +
                ", Model='" + Model + '\'' +
                ", TrackingNo='" + TrackingNo + '\'' +
                ", OrderStatus='" + OrderStatus + '\'' +
                '}';
    }
}
