package jxdeveloping.warehousetool;

import android.support.v7.app.AppCompatActivity;

import jxdeveloping.warehousetool.database.DBHelper;

public class BaseClass extends AppCompatActivity {


    public static MyConfig MyConfig;
    public static DBHelper DBHelper;

    public static MyConfig getMyConfig() {
        return MyConfig;
    }

    public static DBHelper getDBHelper() {
        return DBHelper;
    }
}
