package jxdeveloping.warehousetool.database;

import android.text.TextUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jxdeveloping.warehousetool.BaseClass;

/**
 * Created by justinhsu on 15/08/2017.
 */
public class BaseDBHelper extends BaseClass {
    Connection connect = null;
    ResultSet resultSet = null;
    Statement statement = null;
    PreparedStatement preparedStatement = null;
    DBSource dbsource = null;



    String formatDateTime(String time) {
        try {
//            System.out.println("Input Date : "+time);
            SimpleDateFormat sdf1 = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss'Z'");
            SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf5 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            SimpleDateFormat sdf6 = new SimpleDateFormat("yyyyMMdd");

            Pattern pattern1 = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2}");  //ex: 21.08.2017 09:53:34 Europe/Berlin
            Pattern pattern2 = Pattern.compile("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}");   //ex: 2017-09-03T09:32:18+00:00
            Pattern pattern3 = Pattern.compile("\\d{4}-\\d{2}-\\d{2}T\\d{2}-\\d{2}-\\d{2}Z"); //ex: 2017-09-03T09-32-18Z
            Pattern pattern4 = Pattern.compile("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"); //ex:  2017-08-19 14:58:40 PDT
            Pattern pattern5 = Pattern.compile("\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}"); //ex: 02/08/2017 10:08:12 Europe/London
            Pattern pattern6 = Pattern.compile("\\d{8}"); //ex: 20170424


            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            Matcher matcher = pattern1.matcher(time);
            if (matcher.find()) {
//                System.out.println("match1 : " + matcher.group());
                return output.format(sdf1.parse(matcher.group()));
            }
            matcher = pattern2.matcher(time);
            if (matcher.find()) {
//                System.out.println("match2 : " + matcher.group());
                return output.format(sdf2.parse(matcher.group()));
            }
            matcher = pattern3.matcher(time);
            if (matcher.find()) {
//                System.out.println("match3 : " + matcher.group());
                return output.format(sdf3.parse(matcher.group()));
            }
            matcher = pattern4.matcher(time);
            if (matcher.find()) {
//                System.out.println("match4 : " + matcher.group());
                return output.format(sdf4.parse(matcher.group()));
            }
            matcher = pattern5.matcher(time);
            if (matcher.find()) {
//                System.out.println("match5 : " + matcher.group());
                return output.format(sdf5.parse(matcher.group()));
            }

            matcher = pattern6.matcher(time);
            if (matcher.find()) {
//                System.out.println("match6 : " + matcher.group());
                return output.format(sdf6.parse(matcher.group()));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    public java.sql.Date getDateForDB(String requestDate) {
        if (!TextUtils.isEmpty(requestDate)) {
            try {
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

                Pattern pattern2 = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");   //ex: 2017-09-03T09:32:18+00:00
                Pattern pattern1 = Pattern.compile("\\d{2}/\\d{2}/\\d{4}"); //ex: 02/08/2017 10:08:12 Europe/London
                SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
                Date parsed = null;
                Matcher matcher = pattern1.matcher(requestDate);
                if (matcher.find()) {
//                System.out.println("match1 : "+matcher.group());
                    String match = output.format(sdf1.parse(matcher.group()));
                    parsed = output.parse(match);
                    return new java.sql.Date(parsed.getTime());
                }
                matcher = pattern2.matcher(requestDate);
                if (matcher.find()) {
                    String match = output.format(sdf2.parse(matcher.group()));
                    parsed = output.parse(match);
                    java.sql.Date d = new java.sql.Date(parsed.getTime());
//                    System.out.println("match2 : " + d.toString());
                    return d;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    java.sql.Timestamp getTimestampForDB(String requestDate) {
        if (TextUtils.isEmpty(requestDate)) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parsed = null;
        try {
            parsed = format.parse(requestDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new java.sql.Timestamp(parsed.getTime());
    }

    java.sql.Timestamp getTimestampForDB(long requestDate) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date parsed = null;
//        try {
//            parsed = format.parse(requestDate);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        return new java.sql.Timestamp(requestDate);
    }

    int calculateTotalFulfillableQuantity(int instock, int inboundShipped, int inboundReceving) {
        /** the formula has to be fixed. inboundshipped should divide by shipped days **/
        return instock + inboundReceving + (inboundShipped);
    }


}
