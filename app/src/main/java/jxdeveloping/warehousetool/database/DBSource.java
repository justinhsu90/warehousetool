package jxdeveloping.warehousetool.database;

/**
 * Created by justinhsu on 02/09/2017.
 */

import java.sql.Connection;
import java.sql.SQLException;

public interface DBSource {
    public Connection getConnection() throws SQLException;

    public void closeConnection(Connection conn) throws SQLException;
}