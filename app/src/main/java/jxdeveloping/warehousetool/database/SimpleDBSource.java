package jxdeveloping.warehousetool.database;

/**
 * Created by justinhsu on 02/09/2017.
 */


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import jxdeveloping.warehousetool.BaseClass;

public class SimpleDBSource extends BaseClass implements DBSource {
    //    private Properties props;
    private String url;
    private String user;
    private String passwd;
    private int max = 100; // 連接池中最大Connection數目
    private List<Connection> connections;

    public SimpleDBSource() throws IOException, ClassNotFoundException {
        try {
            user = MyConfig.getCurrentDatabaseUsername();
            passwd = MyConfig.getCurrentDatabasePassword();
            if (MyConfig.isMySQL()) {
                url = "jdbc:mysql://"
                        + MyConfig.getCurrentDatabaseIP() + ":" + MyConfig.getCurrentDatabasePort() + "/" + MyConfig.getCurrentDatabaseDBName()
                        + "?"
                        + "verifyServerCertificate=false"
                        + "&"
                        + "useSSL=true"
                        + "&"
                        + "characterEncoding=utf8";
                Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

            } else {
                url = "jdbc:mariadb://"
                        + MyConfig.getCurrentDatabaseIP() + ":" + MyConfig.getCurrentDatabasePort() + "/" + MyConfig.getCurrentDatabaseDBName()
                        + "?"
                        + "verifyServerCertificate=false"
                        + "&"
//                    + "useSSL=true"
//                    + "&"
                        + "characterEncoding=utf8";
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        connections = new ArrayList<Connection>();
    }

    public synchronized Connection getConnection()
            throws SQLException {
        if (connections.size() == 0) {
//            Connection connection = DriverManager.getConnection(url, user, passwd);
            Properties info = new Properties();
            if (!MyConfig.isProduction() || (!MyConfig.isProduction() && MyConfig.isUseProxy())) {
//                info.put("proxy_type", "4"); // SSL Tunneling
                info.put("proxy_host", MyConfig.getProxyURL());
                info.put("proxy_port", MyConfig.getProxyPort());
            }
            info.put("user", user);
            info.put("password", passwd);
            return DriverManager.getConnection(url, info);
//            return DriverManager.getConnection(url, user, passwd);
        } else {
            int lastIndex = connections.size() - 1;
            return connections.remove(lastIndex);
        }
    }

    public synchronized void closeConnection(Connection conn)
            throws SQLException {
        conn.close();
    }
}