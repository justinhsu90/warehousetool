package jxdeveloping.warehousetool;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jxdeveloping.warehousetool.database.DBHelper;

public class MainActivity extends BaseClass
        implements NavigationView.OnNavigationItemSelectedListener, getDataFromDbAsync.AsyncResponse {
    public static List<WowcherObject> wowcherList = new ArrayList<>();
    public static List<ScannedObject> scannedObjectList = new ArrayList<>();
    public static List<ScannedObject> errorObjectList = new ArrayList<>();
    EditText mTrackingNumber;
    RelativeLayout backgroundView;
    LinearLayout childListView;
    //    TextView wowcherCode;
//    TextView productName;
//    TextView redeemdAt;
    TextView lastupdate;
    //    TextView errorMsg;
//    TextView productNameChinese;
//    TextView spec;
    private final static int INTERVAL = 1000 * 60 * 1; //2 minutes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyConfig = new MyConfig();
        try {
            MyConfig.setCountryAccountIP("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
        );
        DBHelper = new DBHelper();
        setContentView(R.layout.activity_main);
        backgroundView = (RelativeLayout) findViewById(R.id.background);
//        productName = (TextView) findViewById(R.id.productname_text);
//        productNameChinese = (TextView) findViewById(R.id.productname_chinese);
//        spec = (TextView) findViewById(R.id.spec);
        childListView = (LinearLayout) findViewById(R.id.child_body);
//        wowcherCode = (TextView) findViewById(R.id.wowchercode_text);
//        redeemdAt = (TextView) findViewById(R.id.redeemedat_text);
        lastupdate = (TextView) findViewById(R.id.last_db_fetch_time);
//        errorMsg = (TextView) findViewById(R.id.error_type);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTrackingNumber = (EditText) findViewById(R.id.input_tracking_number);
//        mTrackingNumber.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View view, int i, KeyEvent keyEvent) {
////                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER
////                        || keyEvent.getKeyCode() == KeyEvent.KEYCODE_TAB) {
////                     cleanAllField();
////                    if (mTrackingNumber.getText().length() == 13) {
//////                    Snackbar.make(view, "Wowcher data fetched : " + wowcherList.size() + " orders in total.", Snackbar.LENGTH_LONG)
//////                                            .setAction("Action", null).show();
////                        Toast.makeText(MainActivity.this, mTrackingNumber.toString(), Toast.LENGTH_SHORT).show();
////                        //call to do work;
////                        validateData(mTrackingNumber.toString());
////                        mTrackingNumber.selectAll();
//////                } else if (s.length() == 1) {
//////                    System.out.println("else if 0 s.Length is " + s.length());
//////                } else {
//////                    System.out.println(" else s.Length is " + s.length());
//////                    mTrackingNumber.setText("");
////                    }
////                    finish();
////                }
//                return true;
//            }
//        });
        mTrackingNumber.requestFocus();
        mTrackingNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
                v.requestFocus();
//                }
//                mTrackingNumber.setSelectAllOnFocus(true);
                ((EditText) v).setSelectAllOnFocus(true);

            }
        });
        mTrackingNumber.setInputType(InputType.TYPE_NULL);
        mTrackingNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                s.toString().isEmpty();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                cleanAllField();
                if (s.length() == 13) {
//                    Snackbar.make(view, "Wowcher data fetched : " + wowcherList.size() + " orders in total.", Snackbar.LENGTH_LONG)
//                                            .setAction("Action", null).show();
                    Toast.makeText(MainActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
                    //call to do work;
                    validateData(s.toString());
                    mTrackingNumber.selectAll();
//                } else if (s.length() == 1) {
//                    System.out.println("else if 0 s.Length is " + s.length());
//                } else {
//                    System.out.println(" else s.Length is " + s.length());
//                    mTrackingNumber.setText("");
                }
            }
        });
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(MainActivity.this, "Start to fetch data every " + (INTERVAL / 1000 / 60) + " mins", Toast.LENGTH_SHORT).show();
                startRepeatingTask();
                return false;

            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTrackingNumber.requestFocus();

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(MainActivity.this, "Stp to fetch data", Toast.LENGTH_SHORT).show();
            stopRepeatingTask();

            return true;
        } else if (id == R.id.clean_all) {
            mTrackingNumber.setText("");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


//    public void validateData(String trackingNo) {
////        mTrackingNumber.setSelection(0);
////        mTrackingNumber.requestFocus();
////        backgroundView.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));
//        ScannedObject scanObj = new ScannedObject();
//        scanObj.setTrackingNo(trackingNo);
//        scanObj.setTimestamp(System.currentTimeMillis());
//        List<WowcherObject> ordersInOneShipmentList = new ArrayList<>();
//        if (wowcherList == null || wowcherList.isEmpty()) {
//            Toast.makeText(MainActivity.this, "get wowcher order list first", Toast.LENGTH_LONG).show();
//        } else {
//            for (WowcherObject obj : wowcherList) {
//                if (!TextUtils.isEmpty(obj.getTrackingNo()) && obj.getTrackingNo().contains(trackingNo)) {
//                    scanObj.setWowcherCode(obj.getWowcherCode());
//                    setBaseData(obj);
//                    String orderStatus = obj.getOrderStatus();
//                    if (!TextUtils.isEmpty(orderStatus)) {
//                        if (orderStatus.contains("Scan to Ship")) {
//                            setErrorMsg("同單號已發過(1)");
//                            // error
//                            errorObjectList.add(scanObj);
//                            new updateDataFromDbAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                            scannedObjectList.add(scanObj);
//                            System.out.println("2");
//                            setUI(false);
//                        } else if (orderStatus.contains("Shipped")) {
//                            setErrorMsg("Order Shipped Before.");
//
//                            // error
//                            errorObjectList.add(scanObj);
//                            new updateDataFromDbAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                            scannedObjectList.add(scanObj);
//                            System.out.println("2");
//                            setUI(false);
//                        } else {
//                            boolean notFound = true;
//                            for (ScannedObject checkScanObj : scannedObjectList) {
//                                System.out.println("1");
//                                if (scanObj.getTrackingNo().equals(checkScanObj.getTrackingNo()) && !scanObj.getWowcherCode().equals(checkScanObj.getWowcherCode())) {
//                                    notFound = true;
////                                    ordersInOneShipmentList.add()
//                                } else if (scanObj.getWowcherCode().equals(checkScanObj.getWowcherCode())) {
//                                    setErrorMsg("同Wowcher Code已發過貨");
//                                    System.out.println("2");
//                                    scannedObjectList.add(scanObj);
//                                    errorObjectList.add(scanObj);
//                                    new updateDataFromDbAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                                    setUI(false);
//                                    notFound = false;
//                                    break;
//                                }
//                            }
//                            if (notFound) {
//                                setErrorMsg("");
//                                scannedObjectList.add(scanObj);
//                                new updateDataTODbWowcherAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                                setUI(true);
//                                break;
//                            }
//                            //valid with local list to see if it's scanned
//                        }
//                    } else {
//                        setErrorMsg("");
//                        scannedObjectList.add(scanObj);
//                        new updateDataTODbWowcherAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                        setUI(true);
//                        break;
//                    }
//                }
//            }
//            mTrackingNumber.requestFocus();
//        }
//    }


    public void validateData(String trackingNo) {
        ScannedObject scanObj = new ScannedObject();
        scanObj.setTrackingNo(trackingNo);
        scanObj.setTimestamp(System.currentTimeMillis());
        List<WowcherObject> ordersInOneShipmentList = new ArrayList<>();
        if (wowcherList == null || wowcherList.isEmpty()) {
            Toast.makeText(MainActivity.this, "get wowcher order list first", Toast.LENGTH_LONG).show();
        } else {
            for (WowcherObject obj : wowcherList) {
                //DB Wowcher list 的 Tracking Info 是不是等於掃瞄出來的
                if (!TextUtils.isEmpty(obj.getTrackingNo()) && obj.getTrackingNo().contains(trackingNo)) {
                    ordersInOneShipmentList.add(obj);
                }
            }
            if (ordersInOneShipmentList != null && !ordersInOneShipmentList.isEmpty()) {
                setChild(ordersInOneShipmentList);
//                    scanObj.setWowcherCode(obj.getWowcherCode());
//                    setBaseData(obj);
//                    String orderStatus = obj.getOrderStatus();
                // 若Order Status 不為空
//                    if (!TextUtils.isEmpty(orderStatus)) {
//                        if (orderStatus.contains("Scan to Ship")) {
//                            setErrorMsg("同單號已發過(1)");
//                            // error
//                            errorObjectList.add(scanObj);
//                            new updateDataFromDbAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                            scannedObjectList.add(scanObj);
//                            setUI(false);
//                        } else if (orderStatus.contains("Shipped")) {
//                            setErrorMsg("Order Shipped Before.");
//                            // error
//                            errorObjectList.add(scanObj);
//                            new updateDataFromDbAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                            scannedObjectList.add(scanObj);
//                            setUI(false);
//
//                         // 待寄出狀態
//                        } else {
//                            boolean notFound = true;
//                            //檢查之前是否有掃描過
//                            for (ScannedObject checkScanObj : scannedObjectList) {
////                                if (scanObj.getTrackingNo().equals(checkScanObj.getTrackingNo()) && !scanObj.getWowcherCode().equals(checkScanObj.getWowcherCode())) {
////                                    notFound = true;
////                                    ordersInOneShipmentList.add(obj);
////                                } else
//                                    if (scanObj.getWowcherCode().equals(checkScanObj.getWowcherCode())) {
//                                    setErrorMsg("同Wowcher Code已發過貨");
//                                    System.out.println("2");
//                                    scannedObjectList.add(scanObj);
//                                    errorObjectList.add(scanObj);
//                                    new updateDataFromDbAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                                    setUI(false);
//                                    notFound = false;
//                                    break;
//                                }
//                            }
//                            if (notFound) {
//                                setErrorMsg("");
//                                scannedObjectList.add(scanObj);
//                                new updateDataTODbWowcherAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                                setUI(true);
//                                break;
//                            }
//                            //valid with local list to see if it's scanned
//                        }
//                    } else {
//                        setErrorMsg("");
//                        scannedObjectList.add(scanObj);
//                        new updateDataTODbWowcherAsync(MainActivity.this, scanObj.getWowcherCode(), scanObj.getTrackingNo()).execute();
//                        setUI(true);
//                        break;
//                    }
//                }

            }
        }
        mTrackingNumber.requestFocus();
    }


    private void setChild(List<WowcherObject> list) {
//        childListView
//                RelativeLayout child = new RelativeLayout();

//        child = (RelativeLayout) findViewById(R.layout.activity_main);

//        TextView engName = (TextView) view.findViewById(R.id.);
        for (WowcherObject obj : list) {
            View view = LayoutInflater.from(this).inflate(R.layout.item, null);
            TextView wowcher = (TextView) view.findViewById(R.id.wowchercode_child);
            wowcher.setText(obj.getWowcherCode());
            childListView.addView(view);
        }

    }

    private void setUI(boolean isGood) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isGood) {
//                    errorMsg.setBackground(new ColorDrawable(Color.TRANSPARENT));
                    backgroundView.setBackground(new ColorDrawable(Color.parseColor("#7CFC00")));

                } else {
//                    errorMsg.setBackground(new ColorDrawable(Color.parseColor("#ff69b4")));
                    backgroundView.setBackground(new ColorDrawable(Color.parseColor("#FFB6C1")));
                }
                mTrackingNumber.requestFocus();
            }
        });

    }

    private void setErrorMsg(String message) {
//        errorMsg.setText(message);
        mTrackingNumber.requestFocus();

    }

    @Override
    public void processFinish(List<WowcherObject> output) {
        System.out.println("in processFinish: " + output);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日HH:mm:ss");

        Date curDate = new Date(System.currentTimeMillis()); // 獲取當前時間

        String str = formatter.format(curDate);
        lastupdate.setText(str);
        if (output != null && !output.isEmpty()) {
            wowcherList = output;
            Snackbar.make(findViewById(R.id.fab), "Wowcher data fetched : " + output.size() + " orders in total.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            Snackbar.make(findViewById(R.id.fab), "Wowcher data fetched Fail ", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    public void setBaseData(WowcherObject obj) {
//        wowcherCode.setText(obj.getWowcherCode());
//        productName.setText(obj.getProductName());
//        productNameChinese.setText(getProductNameInChinese(obj.getProductName()));
//        redeemdAt.setText(obj.getRedeemedAt());
//        spec.setText(getColorInChinese(obj.getModel()));
    }

    public void cleanAllField() {
        backgroundView.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));
//        wowcherCode.setText("");
//        productName.setText("");
//        redeemdAt.setText("");
//        errorMsg.setText("");
//        productNameChinese.setText("");
//        spec.setText("");

    }


    public String getColorInChinese(String str) {
        switch (str) {
            case "Black":
                return "黑色";
            case "Pink":
                return "深粉紅";
            case "White":
                return "白";
            case "Purple":
                return "紫色";
            case "Yellow":
                return "黃色";
            case "Green":
                return "Green";
            case "Animal":
                return "動物";
            case "Vehicle":
                return "交通工具";
            case "Red":
                return "紅色";
            case "Rosepink":
                return "卡其色/淺粉紅/玫瑰粉";
            case "Blue":
                return "藍色";
            case "Turquoise":
                return "藍綠色";

        }
        return "";
    }


    public String getProductNameInChinese(String str) {
        switch (str) {
            case "An advanced turbocharged pressure shower":
                return "花灑";
            case "A rose gold-plated portable facial hair remover":
                return "除毛機";
            case "A 17-in-1 waterproof smart fitness bracelet":
                return "手環";
            case "A pair of Apple-compatible wireless earbuds with charging case":
                return "帶盒耳機";
            case "A digital wrist blood pressure monitor":
                return "血壓儀";
            case "A resistance band upper body workout machine":
                return "Wonder Arm 訓練器";
            case "A pair of wireless Bluetooth headphones":
                return "帶線藍牙耳機";
//            case "Vehicle":
//                return "交通工具";
//            case "Red":
//                return "紅色";
//            case "Rosepink":
//                return "卡其色/淺粉紅/玫瑰粉";
//            case "Blue":
//                return "藍色";
//            case "Turquoise":
//                return "藍綠色";

        }
        if (str.contains("anti-slip touchscreen sports gloves")) {
            return "運動手套";
        }
        return "";
    }

    Handler mHandler = new Handler();
    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            new getDataFromDbAsync(MainActivity.this, findViewById(R.id.fab), MainActivity.this).execute();
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    void startRepeatingTask() {
        mHandlerTask.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mHandlerTask);
    }


}
