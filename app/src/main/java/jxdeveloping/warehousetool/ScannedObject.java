package jxdeveloping.warehousetool;

/**
 * Created by justinhsu on 29/05/2017.
 */
public class ScannedObject {

    String trackingNo = "";
    String wowcherCode = "";
    long timestamp = 0;

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    public String getWowcherCode() {
        return wowcherCode;
    }

    public void setWowcherCode(String wowcherCode) {
        this.wowcherCode = wowcherCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
