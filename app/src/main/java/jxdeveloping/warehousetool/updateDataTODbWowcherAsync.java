package jxdeveloping.warehousetool;

import android.content.Context;
import android.os.AsyncTask;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by justinhsu on 13/01/2018.
 */

public class updateDataTODbWowcherAsync extends AsyncTask<Void, Void, List<WowcherObject>> {
    //        ProgressDialog mProgressDialog;
    public AsyncResponse delegate = null;
    Context context;
    //    View view;
    String tracking;
    String wowcher;
    String url = "jdbc:mariadb://"
            + "61.216.178.44:3307/CMS"
            + "?"
            + "verifyServerCertificate=false"
            + "&"
//                    + "useSSL=true"
//                    + "&"
            + "characterEncoding=utf8";

    public interface AsyncResponse {
        void processFinish(List<WowcherObject> output);
    }

    public updateDataTODbWowcherAsync(Context context, String wowcher, String tracking) {
        this.context = context;
//        this.view = view;
//        this.delegate = delegate;
        this.wowcher = wowcher;
        this.tracking = tracking;
    }

    protected void onPreExecute() {
//        Snackbar.make(view, "Fetching Wowcher Data from DB", Snackbar.LENGTH_SHORT)
//                .setAction("Action", null).show();
    }

    protected List<WowcherObject> doInBackground(Void... params) {
        List<WowcherObject> list = new ArrayList<>();
        try {
            try {
                Class.forName("org.mariadb.jdbc.Driver").newInstance();
//                    Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

//                Class.forName("org.mariadb.jdbc.Driver");
            String user = MyConfig.getCurrentDatabaseUsername();
            String passwd = MyConfig.getCurrentDatabasePassword();
            java.sql.Connection con = DriverManager.getConnection(url, "master", "Fbtw.2016");
//            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//            long time = System.currentTimeMillis();
//            Date date = new Date();
//            date.setTime(time * 1000);
//            System.out.println("time stamp is " + time);
            String queryStr = "UPDATE WowcherOrderList SET OrderStatus='Scan to Ship' WHERE TrackingNo = '" + tracking + "'";

            java.sql.Statement st = con.createStatement();

            java.sql.ResultSet rs = st.executeQuery(queryStr);
//            WowcherObject obj;
//            while (rs.next()) {
//                obj = new WowcherObject();
//                obj.setWowcherCode(rs.getString("WowcherCode"));
//                obj.setRedeemedAt(rs.getString("RedeemedAt"));
//                obj.setOrderStatus(rs.getString("OrderStatus"));
//                obj.setTrackingNo(rs.getString("TrackingNo"));
//                obj.setProductName(rs.getString("ProductName"));
//                list.add(obj);
//            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

        }
        System.out.println("get getWowcherShippingStatusData done");
        return list;
    }

    protected void onPostExecute(List<WowcherObject> result) {
//        delegate.processFinish(result);

    }


//    java.sql.Timestamp getTimestampForDB() {
////        if (TextUtils.isEmpty(requestDate)) {
////            return null;
////        }
//        Date date1 = new Date(System.currentTimeMillis() * 1000L);
//
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date parsed = null;
//        try {
//            parsed = format.parse(date1);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return new java.sql.Timestamp(parsed.getTime());
//    }

}